#Download base image ubuntu 18.04
FROM ubuntu:18.04

# LABEL about the custom image
LABEL version="0.1"
LABEL description="This is custom Docker Image for \
the app deployment."

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

# Install from ubuntu repository
RUN apt-get update && apt-get install -y libicu-dev &&\
 apt-get update && apt install -y python3-pip &&\
 apt-get update && apt install -y curl &&\
 apt-get update && apt install -y openjdk-8-jdk

RUN ln -s /usr/bin/python3 /usr/bin/python &&\
	ln -s /usr/bin/pip3 /usr/bin/pip

RUN apt-get clean && apt-get update && apt-get install -y locales
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en

COPY requirements.txt /app/

RUN pip install pyicu && pip install -r /app/requirements.txt && python -m spacy download en_core_web_sm
RUN pip install --upgrade pip
RUN pip install grpcio==1.28.1

ENV GOOGLE_APPLICATION_CREDENTIALS /app/MyFirstProject-04102a332037.json
RUN echo "deb [arch=amd64] http://storage.googleapis.com/tensorflow-serving-apt stable tensorflow-model-server tensorflow-model-server-universal" | tee /etc/apt/sources.list.d/tensorflow-serving.list && \
curl https://storage.googleapis.com/tensorflow-serving-apt/tensorflow-serving.release.pub.gpg | apt-key add -
RUN apt-get update && apt-get install -y tensorflow-model-server && apt-get upgrade -y tensorflow-model-server 

EXPOSE 80
WORKDIR /app
CMD python tfserving.py && gunicorn -c gunicorn_config.py app:server



