#!/bin/bash
docker-compose up -d
sleep 10
npm install -g localtunnel
nohup lt --port 80 --subdomain $1 > url 2>&1 &
_pid=$!
read -r -d '' _ </dev/tty
ctrl_c()
{
	docker-compose down && kill -9 $_pid
	exit
}
trap ctrl_c SIGINT
